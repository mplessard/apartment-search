""" Apartment search is a program developed to sort some kijiji's apartment ads by metro station and walk distance.
    Copyright (C) 2017  Marie-Pier Lessard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""

import sys

from app.sqlalchemy_db import session, LineStation
from app.subway_station_search import LINES, search_stations_lines, search_stations
from app.apartment_search import search_kijiji

lines = [s[0] for s in session.query(LineStation.name).distinct()]

if lines != LINES:
    print('looking for subway stations...')
    lines = search_stations_lines()

    while(len(lines) != 0):
        lines = search_stations(lines)

print('looking for kijiji\'s ads...')
number_args = len(sys.argv)

try:
    if number_args == 1:
        search_kijiji()
    elif number_args == 3:
        search_kijiji(int(sys.argv[1]), int(sys.argv[2]))
    elif number_args == 4:
        search_kijiji(int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]))
    else:
        print('You must enter 0, 2 or 3 parameters in order to run the program.')
        exit(-1)
except ValueError:
    print('You must only enter numbers as parameters.')
    exit(-1)
