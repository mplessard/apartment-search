""" Apartment search is a program developed to sort some kijiji's apartment ads by metro station and walk distance.
    Copyright (C) 2017  Marie-Pier Lessard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""

import requests
from bs4 import BeautifulSoup

from app.sqlalchemy_db import session, Station, LineStation, add_and_commit
from app.utilities import get_google_coordinates


STM_URL = 'http://www.stm.info/'
STM_METRO_URL = '{}fr/infos/reseaux/metro/'.format(STM_URL)

LINES = ['bleue', 'jaune', 'orange', 'verte']


def search_stations_lines(lines=LINES):
    for index, line in enumerate(lines):
        print('searching \'{}\' line.'.format(line))

        try:
            search_stations(line)
        except requests.RequestException:
            return lines[index:]

    return []


def search_stations(line):
    request = requests.get('{}{}'.format(STM_METRO_URL, line))
    html = BeautifulSoup(request.text, 'html.parser')
    links = html.find('ol', id='metro-stations-list').find_all('a')

    for link in links:
        name, url = parse_station_info(link)
        add_station(name, url, line)


def parse_station_info(link):
    name = link.contents[1].strip()
    url = STM_URL + link['href']

    return name, url


def add_station(name, url, line):
    existing_station = session.query(Station).filter_by(name=name).one_or_none()

    if existing_station is None:
        station = create_station(name, url)
        add_and_commit(station)

        line_station = LineStation(name=line, station_id=station.id)
    else:
        line_station = LineStation(name=line, station_id=existing_station.id)

    add_and_commit(line_station)


def create_station(name, url):
    station = Station(name=name, url=url)
    station.latitude, station.longitude = get_google_coordinates('Station {}'.format(name))

    return station
