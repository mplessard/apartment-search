""" Apartment search is a program developed to sort some kijiji's apartment ads by metro station and walk distance.
    Copyright (C) 2017  Marie-Pier Lessard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""


import os
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.orm import sessionmaker


basedir = os.path.abspath(os.path.dirname(__file__))
engine = create_engine('sqlite:///' + os.path.join(basedir, 'apartment_search.db'))
Base = declarative_base()


class Ad(Base):
    __tablename__ = 'ad'
    id = Column(Integer, primary_key=True)
    url = Column(Text, unique=True)
    creation_date = Column(Date)
    price = Column(Float)
    address = Column(String(250))
    description = Column(Text)
    bathroom = Column(Integer)
    pets = Column(Boolean)
    furnished = Column(Boolean)
    latitude = Column(Float)
    longitude = Column(Float)
    size = Column(String)
    nearby_stations = relationship('NearbyStation')

    def __repr__(self):
        return '<Ad id={}, url={}, price={}, address={}>'.format(self.id, self.url, self.price, self.address)


class Image(Base):
    __tablename__ = 'image'
    id = Column(Integer, primary_key=True)
    ad_id = Column(Integer, ForeignKey('ad.id'))
    src = Column(Text)
    ad = relationship('Ad', backref=('image'))

    def __repr__(self):
        return'<Image id={}, ad_id={}, src={}>'.format(self.id, self.ad_id, self.src)


class LineStation(Base):
    __tablename__ = 'line_station'
    id = Column(Integer, primary_key=True)
    name = Column(String(10))
    station_id = Column(Integer, ForeignKey('station.id'))
    station = relationship('Station')


class NearbyStation(Base):
    __tablename__ = 'nearby_station'
    id = Column(Integer, primary_key=True)
    ad_id = Column(Integer, ForeignKey('ad.id'))
    station_id = Column(Integer, ForeignKey('station.id'))
    ad = relationship('Ad')
    station = relationship('Station')
    walk_time = Column(Integer)

class Station(Base):
    __tablename__ = 'station'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True)
    url = Column(Text)
    latitude = Column(Float)
    longitude = Column(Float)
    line = relationship('LineStation')

    def __repr__(self):
        return '<Station id={}, name={}, url={}>'.format(self.id, self.name, self.url)


Base.metadata.create_all(engine)
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()


def add_and_commit(object):
    session.add(object)
    session.commit()
