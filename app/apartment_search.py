""" Apartment search is a program developed to sort some kijiji's apartment ads by metro station and walk distance.
    Copyright (C) 2017  Marie-Pier Lessard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""


import datetime
import re

import requests
from bs4 import BeautifulSoup

from app.sqlalchemy_db import Ad, Image, session, add_and_commit
from app.utilities import get_google_coordinates, find_nearby_station


WEBSITE_URL = 'http://www.kijiji.ca'
WEBSITE_APARTMENT_URL = '{}/b-appartement-condo/ville-de-montreal/page-{}/c37l1700281?price={}__{}&minNumberOfImages=1'

APARTMENT_SIZES = {'v-bachelor-studio-apartments-condos': '1 1/2',
                   'v-1-bedroom-apartments-condos': '2 1/2',
                   'v-2-bedroom-apartments-condos': '4 1/2',
                   'v-appartement-condo-studio-1-1-2': '1 1/2',
                   'v-appartement-condo-studio-2-1-2': '2 1/2',
                   'v-appartement-condo-3-1-2': '3 1/2',
                   'v-appartement-condo-4-1-2': '4 1/2',
                   'v-appartement-condo-5-1-2': '5 1/2',
                   'v-appartement-condo-6-1-2-et-plus': '6 1/2 et plus'}


def find_pages_urls(min_price, max_price, max_pages):
    urls = []
    page_number, last_page = 1, 1

    while page_number <= last_page:
        print('search page {}'.format(page_number))

        request_url = WEBSITE_APARTMENT_URL.format(WEBSITE_URL, page_number, min_price, max_price)
        html = get_html(request_url)

        if html is not False:
            urls.extend(find_ads_urls(html))

            last_page = compare_last_pages(find_last_page_in(html), last_page, max_pages)

        page_number += 1

    return list(set(urls))


def get_html(request_url):
    try:
        request = requests.get(request_url)
        html = BeautifulSoup(request.text, 'html.parser')

        return html
    except requests.RequestException:
        print('Error: GET {} has failed.'.format(request_url))

        return False


def find_ads_urls(html):
    urls = html.find_all('a', class_='title enable-search-navigation-flag ')

    ads_url_refined = []
    for url in urls:
        url = WEBSITE_URL + url.get('href')

        if len(session.query(Ad).filter_by(url=url).all()) == 0:
            ads_url_refined.append(url)

    return ads_url_refined


def compare_last_pages(last_page_in_page, actual_last_pages_counter, max_pages):
    if actual_last_pages_counter <= last_page_in_page <= max_pages:
        return last_page_in_page

    elif last_page_in_page > max_pages:
        return max_pages

    else:
        return actual_last_pages_counter


def find_last_page_in(html):
    numbers = html.find('div', class_='pagination').find_all('a')
    page = int(numbers[-3].string)

    return page


def find_ads_infos(url):
    print(url)
    html = get_html(url)
    infos = html.find('table', class_='ad-attributes')

    if infos is not None:
        infos = infos.find_all('tr')

        ad_infos = find_info_by_titles(infos)
        ad_infos['description'] = find_ad_description(html)

        images = html.find('div', id='ImageThumbnails')

        ad_images = []
        if images is not None:
            images = images.find_all('img')

            for image in images:
                ad_images.append(image['src'].replace('$_14.JPG', '$_27.JPG'))

        return ad_infos, ad_images
    else:
        return False, False



def find_info_by_titles(infos):
    ad_infos = {}

    for info in infos:
        if info.th is not None:
            title = info.th.string

            if 'Date' in title:
                ad_infos['date'] = info.find('td').string
            elif 'Prix' in title:
                ad_infos['price'] = (info.find('strong').string.replace(',', '.').replace('\xa0', '').replace('$', ''))

            elif 'Adresse' in title:
                ad_infos['address'] = info.td.contents[0]

            elif 'Salles de bain' in title:
                ad_infos['bathroom'] = info.td.string.strip()[0]

            elif 'Meublé' in title:
                ad_infos['furnished'] = 0 if 'Non' in info.td.string else 1

            elif 'Animaux' in title:
                ad_infos['pets'] = 0 if 'Non' in info.td.string else 1

    return ad_infos


def find_ad_description(html):
    description = html.find('span', {'itemprop': 'description'}).get_text().strip()

    return description


def search_kijiji(min_price='', max_price='', max_pages=100):
    print('Finding all urls...')
    ads_url = find_pages_urls(min_price, max_price, max_pages)

    print('Finding all apartments infos...')
    add_ads(ads_url)


def add_ads(ads_url):
    for url in ads_url:
        ad_info, ad_images = find_ads_infos(url)

        if ad_info is not False and ad_images is not False:
            ad = parse_ad_info(ad_info)
            ad.url = url
            ad.size = find_apartment_size(url)

            if ad.latitude is not None and ad.longitude is not None:
                add_and_commit(ad)

                add_ad_images(ad_images, ad.id)
                find_nearby_station(ad)


def find_apartment_size(url):
    size = url.replace(WEBSITE_URL + '/', '')
    size = re.sub('\/.*', '', size)

    if size in APARTMENT_SIZES:
        return APARTMENT_SIZES[size]
    else:
        return None


def add_ad_images(ad_images, ad_id):
    for image in ad_images:
        image = Image(src=image, ad_id=ad_id)

        add_and_commit(image)


def parse_date(date):
    months = ({'janv.': 1, 'févr.': 2, 'mars': 3, 'avr.': 4, 'mai': 5, 'juin': 6,
               'juil.': 7, 'aout': 8, 'sept.': 9, 'oct.': 10, 'nov.': 11, 'déc.': 12})

    date = date.replace('<td>', '').replace('</td>', '')
    date = date.split('-')

    day = int(date[0])
    month = months[date[1]]
    year = int('20{}'.format(date[2]))

    return datetime.date(year, month, day)


def parse_price(price):
    try:
        return float(price)
    except ValueError:
        return None


def parse_ad_info(infos):
    ad = Ad()

    ad.creation_date = parse_date(infos['date'])
    ad.price = parse_price(infos['price'])
    ad.address = infos['address']
    ad.description = infos['description']
    
    if 'furnished' in infos:
        ad.furnished = infos['furnished']

    if 'bathroom' in infos:
        ad.bathroom = infos['bathroom']

    if 'pets' in infos:
        ad.pets = infos['pets']

    ad.latitude, ad.longitude = get_google_coordinates(infos['address'])

    return ad
