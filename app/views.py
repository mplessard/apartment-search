""" Apartment search is a program developped to sort some kijiji's apartment ads by metro station and walk distance.
    Copyright (C) 2017  Marie-Pier Lessard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""

import math

from flask import render_template, abort

from app import app
from .forms import SearchForm
from .sqlalchemy_db import Ad, session, NearbyStation


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')


@app.route('/apartments/', methods=['GET', 'POST'])
@app.route('/apartments/<int:page_number>', methods=['GET', 'POST'])
def apartments(page_number=1):
    form = SearchForm()
    if form.validate_on_submit():
        ads = format_search_query(form)

        number_of_pages = math.ceil(ads.count() / 33)
        begin_slice, end_slice = which_slice(page_number)
        # TODO slice ads, but keep track of search.
        
        ads = ads.all()

    else:
        number_of_pages = math.ceil(session.query(Ad).count() / 33)
        begin_slice, end_slice = which_slice(page_number)
        ads = session.query(Ad).order_by(Ad.creation_date.desc()).slice(begin_slice, end_slice)

    return render_template('apartments.html', ads=ads, number_of_pages=number_of_pages, page_number=page_number,
                           form=form)


def which_slice(page_number):
    if page_number is None or page_number <= 1:
        return 0, 33
    else:
        begin_slice = 1 + 33 * (page_number - 1)
        end_slice = 33 * page_number
        return begin_slice, end_slice


def format_search_query(form):
    APARTMENT_SIZE = {
        1: '1 1/2',
        2: '2 1/2',
        3: '3 1/2',
        4: '4 1/2',
        5: '5 1/2',
        6: '6 1/2 et plus'
    }

    size = APARTMENT_SIZE.get(form.apartment_size.data)
    max_price = form.max_price.data
    pets_allowed = form.pets_allowed.data
    furnished = form.furnished.data
    line = form.line.data
    max_walktime = form.max_walktime.data

    ads = session.query(Ad).order_by(Ad.creation_date.desc())

    if size is not None:
       # ads = ads.filter(Ad.size == size)
        pass
    if max_price is not None:
        ads = ads.filter(Ad.price <= max_price)
    if pets_allowed:
        ads = ads.filter(Ad.pets)
    if furnished:
        ads = ads.filter(Ad.furnished)
    if line is not None:
        pass
        # ads = ads.filter(Ad.nearby_stations.station.name == line)
    if max_walktime is not None:
        ads = ads.filter(Ad.nearby_stations.any(NearbyStation.walk_time <= max_walktime * 60))
    return ads


@app.route('/apartment/<int:id>')
def apartment(id):
    ad = session.query(Ad).filter(Ad.id == id).one_or_none()

    if ad is not None:
        return render_template('apartement.html', ad=ad)
    else:
        return abort(404)
