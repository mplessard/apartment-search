""" Apartment search is a program developed to sort some kijiji's apartment ads by metro station and walk distance.
    Copyright (C) 2017  Marie-Pier Lessard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""

from flask_wtf import Form
from wtforms import BooleanField, DecimalField, SelectField, IntegerField, validators

from .subway_station_search import LINES


class SearchForm(Form):
    # TODO stationnement
    # TODO laveuse sécheuse
    APARTMENT_SIZES = [(1, '1 1/2'), (2, '2 1/2'), (3, '3 1/2'), (4, '4 1/2'), (5, '5 1/2'), (6, '6 1/2 et plus')]

    LINE_CHOICES = [(line, line) for line in LINES]

    max_price = DecimalField('max_price', [validators.optional()], places=2)
    apartment_size = SelectField('apartment_type', [validators.optional()], coerce=int, choices=APARTMENT_SIZES)
    pets_allowed = BooleanField('pets_allowed')
    furnished = BooleanField('furnished')

    line = SelectField('subway_line', [validators.optional()], choices=LINE_CHOICES)
    max_walktime = IntegerField('max_walktime', [validators.optional()])

